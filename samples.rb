##
# Samples module
# 
# Methods included define and return filters.
# Use 'test' method to generate binary kernel version of filter
# and apply it to a sample PNG
module Samples
    Filter = ImageFilterDsl::Filter
    def self.rect_grad() 
        Filter.define [:x,:y,:width,:hght], [:r, :g, :b, :a] do
            copy [255], :a
            max [0.1, :x], :t
            float [:t], :t
            div [:t, :width], :t2
            mult [255, :t2], :t2
            copy [:t2], :r
            max [0.1, :y], :t
            float [:t], :t
            div [:t, :hght], :t3
            mult [255, :t3], :t3
            copy [:t3], :g
            avg [:t2, :t3], :t
            copy [:t], :b
        end
    end

    def self.trans_rect_grad() 
        Filter.define [:x,:y,:width,:hght, :a], [:r, :g, :b, :a] do
            copy [:a], :a
            max [0.1, :x], :t
            float [:t], :t
            div [:t, :width], :t2
            mult [255, :t2], :t2
            copy [:t2], :r
            max [0.1, :y], :t
            float [:t], :t
            div [:t, :hght], :t3
            mult [255, :t3], :t3
            copy [:t3], :g
            avg [:t2, :t3], :t
            copy [:t], :b
        end
    end

    def self.line_vert_inv()
        Filter.define [:y, :r, :g, :b, :a], [:r, :g, :b, :a] do
            copy [:a], :a
            mod [:y, 10], :t1
            above [:t1, 3, 1.0, 0.0], :cond
            add [-255,:b], :invb
            add [-255,:g], :invg
            add [-255,:r], :invr
            abs [:invb], :invb
            abs [:invg], :invg
            abs [:invr], :invr
            switch [:cond, :r, :invr], :r
            switch [:cond, :g, :invg], :g
            switch [:cond, :b, :invb], :b
        end
    end

    def self.avg_enhance_red()
        Filter.define [:r,:g,:b,:a], [:r,:g,:b,:a] do
            copy [:a], :a
            # avg g + b, * 2, cap to max of 255
            avg [:g,:b], :tavg
            mult [:tavg, 2], :tavg
            min [:tavg,255], :tavg
            max [:tavg, :r], :tavg
            max [:g, :b], :mgb
            above [:r, :mgb,1,0], :cond
            mult [:g, 0.5], :tg
            mult [:b, 0.5], :tb
            switch [:cond, :tg, :g], :g
            switch [:cond, :tb, :b], :b
            switch [:cond, :tavg, :r], :r
        end
    end

    def self.exaggerate()
        Filter.define [:r,:g,:b,:a],[:r,:g,:b,:a] do
            avg [:r,:g], :avb
            avg [:g,:b], :avr
            avg [:b, :r], :avg
            above [:avb, :b], :cb
            above [:avr, :r], :cr
            above [:avg, :g], :cg
            mult [:avb, 2], :eb
            mult [:avr, 2], :er
            mult [:avg, 2], :eg
            min [:eb, 255], :eb
            min [:er, 255], :er
            min [:eg, 255], :eg
            mult [:r, 0.5], :hr
            mult [:g, 0.5], :hg
            mult [:b, 0.5], :hb
            switch [:cr, :er, :hr], :r
            switch [:cg, :eg, :hg], :g
            switch [:cb, :eb, :hb], :b
            copy [:a], :a
        end
    end

    def self.exaggerate2()
        Filter.define [:r,:g,:b,:a],[:r,:g,:b,:a] do
            avg [:r,:g], :avb
            avg [:g,:b], :avr
            avg [:b, :r], :avg
            below [:avb, :b], :cb
            below [:avr, :r], :cr
            below [:avg, :g], :cg
            mult [:avb, 2], :eb
            mult [:avr, 2], :er
            mult [:avg, 2], :eg
            min [:eb, 255], :eb
            min [:er, 255], :er
            min [:eg, 255], :eg
            switch [:cr, :er, :r], :r
            switch [:cg, :eg, :g], :g
            switch [:cb, :eb, :b], :b
            copy [:a], :a
        end
    end

    def self.exagerate3()
        Filter.define [:r,:g,:b,:a],[:r,:g,:b,:a] do
            max [:r, :g], :m
            max [:m, :b], :m
            eq [:m, :r], :ruse
            eq [:m, :g], :guse
            eq [:m, :b], :buse
            avg [:r,:g], :avb
            avg [:g,:b], :avr
            avg [:b, :r], :avg
            below [:avb, :b], :cb
            below [:avr, :r], :cr
            below [:avg, :g], :cg
            mult [:avb, 2], :eb
            mult [:avr, 2], :er
            mult [:avg, 2], :eg
            min [:eb, 255], :eb
            min [:er, 255], :er
            min [:eg, 255], :eg
            switch [:cr, :er, :r], :rx
            switch [:cg, :eg, :g], :gx
            switch [:cb, :eb, :b], :bx
            switch [:ruse, :rx, :r], :r
            switch [:guse, :gx, :g], :g
            switch [:buse, :bx, :b], :b
            copy [:a], :a
        end
    end

    ##
    # Use this method to apply a filter to the sample spec.png image,
    # and save a binary kernel version of filter to /kernels
    # @param [ImageFilterDsl::Dsl::Kernel::KernelFilter] filter Filter to run
    # @param [String] name Name to use for generated binary kernel (.ifdk) and .png
    def self.test(filt,name)
        ImageFilterDsl.save_binary_kernel(filt,"./kernels/#{name}.ifdk")
        p = ImageFilterDsl.image_processor(filt)
        p.process_image("./media/spec.png", "./out/#{name}_spec.png")
    end
end