# Image Filter DSL Gem Samples

## Usage

### Requirements

Requires `image_filter_dsl` gem to be installed. Check out the repo and build a copy [Repo](https://bitbucket.org/WadeH/image_filter_dsl)


### Running samples

Reference `samples.rb` from your code, or load it into IRB or pry.

Run `Samples.test(<filter>,<output name>)` to build a binary kernel version of a filter and apply it to a sample PNG.

`Samples` module includes a number of methods that return filter kernels.

## Sample Listing

Samples included in `samples.rb`

- `rect_grad` - Fills output image with rectangular gradient
- `trans_rect_grad` - Fills output image with rectangular gradient, preserving source alpha
- `line_vert_inv` - adds vertical lines of inverted color
- `avg_enhance_red` - enhances reds based on averages
- `exaggerate`, `exaggerate2`, `exaggerate3` - Exaggerates colors